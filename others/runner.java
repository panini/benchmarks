/*
 * This file is part of the Panini project at Iowa State University.
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/.
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 * 
 * For more details and the latest version of this code please see
 * http://paninij.org
 * 
 * Contributor(s): Ganesha Upadhyaya
 */

import java.util.*;

capsule RunActor(Worker workers[]) {
	int memAcc = 100;
	
	void init(int localSize) {
		for(int i = 0; i < workers.length; i++) {
			workers[i].init(localSize);
		}
	}
	
	long run(int iter) {
		List<Integer> res = new ArrayList<Integer>();
		long tim = System.currentTimeMillis();
		for(int i = 0; i < workers.length; i++) {
			for(int count = 0; count < iter/workers.length; count++)
				res.add(workers[i].doWork(memAcc));
		}
		int sum = 0;
		for (Integer i : res)
			sum += i;
		long dur = System.currentTimeMillis()-tim;
		return dur;
	}
}

capsule Worker {
	Random rand = new Random(13);
	int localState[];
	
	void init(int localSize) {
		localState = new int[localSize];
	}
	
	int doWork(int iterations) {
		int sum = 0;
		for(int i = 0; i < iterations; i++)  {
			int index = rand.nextInt(localState.length);
			sum += localState[index];
			localState[index] = i;
		}
		return sum;
	}
}

capsule runner {
	int MAX_WORKER = 8;
	int NUM_ACTORS_PER_WORKER = 10;
	design {
		Worker workers[80]; // MAX_WORKER * NUM_ACTORS_PER_WORKER
		RunActor r;
		r(workers);
	}
	
	void run() {
		int sizes[] = {
				16,
				64,
				500,
				1000,
				8000,
				80000
		};
		long durations[][] = new long[sizes.length][];
		for (int i = 0; i < sizes.length; i++) {
			int size = sizes[i];
			for ( int ii = 0; ii < 2; ii++ ) {
				System.out.println("warmup =>");
				avgTest(size);
			}
			durations[i] = new long[1];
			int numRuns = 3;
			for ( int ii = 0; ii < numRuns; ii++ ) {
				System.out.println("run => "+ii);
				durations[i][0] += avgTest(size);
			}
			durations[i][0] /= numRuns;
		}
		System.out.println("Final results ************** Worker Threads:"+MAX_WORKER+" actors:"+(MAX_WORKER*NUM_ACTORS_PER_WORKER));
		for (int i = 0; i < durations.length; i++) {
			long[] duration = durations[i];
			System.out.println("local state bytes: "+sizes[i]*4+" "+" duration:"+duration[0]);
		}
	}
	
	private long avgTest(int localSize) {
		long sum = 0;
		int iters = 1;
		r.init(localSize);
		for (int i = 0; i < iters; i++) {
			sum += r.run(1000 * 1000 * 5);
		}
		//r.done();
		System.out.println("*** average "+sum/iters+" localSize "+localSize*4);
		return sum/iters;
	}
	
	private void init() {
		
	}
}