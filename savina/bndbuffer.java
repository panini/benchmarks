/*
 * This file is part of the Panini project at Iowa State University.
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/.
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 * 
 * For more details and the latest version of this code please see
 * http://paninij.org
 * 
 * Contributor(s): Ganesha Upadhyaya
 */

import java.util.*;
class ProdConsBoundedBufferConfig {
	protected static int bufferSize = 50;
	protected static int numProducers = 40;
	protected static int numConsumers = 40;
	protected static int numItemsPerProducer = 1000;//1_000;
	protected static int prodCost = 25;
	protected static int consCost = 25;
	protected static int numMailboxes = 1;
	protected static boolean debug = false;

	protected static void parseArgs(final String[] args) {

	}

	protected static void printArgs() {

	}

	protected static double processItem(final double curTerm, final int cost) {
		double res = curTerm;
		final Random random = new Random(cost);
		
		if (cost > 0) {
			for (int i = 0; i < cost; i++) {
				for (int j = 0; j < 100; j++) {
					res += Math.log(Math.abs(random.nextDouble()) + 0.01);
				}
			}
		} else {
			res += Math.log(Math.abs(random.nextDouble()) + 0.01);
		}
		return res;
	}

	protected enum MessageSource {
		PRODUCER,
		CONSUMER
	}

	protected static class DataItemMessage {
		public final double data;
		public final int producerId;

		DataItemMessage(final double data, final int producerId) {
			this.data = data;
			this.producerId = producerId;
		}
	}

	protected static class ProduceDataMessage {
		protected static ProduceDataMessage ONLY = new ProduceDataMessage();
	}	

	protected static class ProducerExitMessage {
		protected static ProducerExitMessage ONLY = new ProducerExitMessage();
	}

	protected static class ConsumerAvailableMessage {
		public final int consumerId;
		
		ConsumerAvailableMessage(final int consumerId) {
			this.consumerId = consumerId;
		}
	}

	protected static class ConsumerExitMessage {
		protected static ConsumerExitMessage ONLY = new ConsumerExitMessage();
	}
}

capsule ProducerActor (int id, ManagerActor manager, int numItemsToProduce) {
	double prodItem = 0.0;
	int itemsProduced = 0;

	private void produce() {
		prodItem = ProdConsBoundedBufferConfig.processItem(prodItem, ProdConsBoundedBufferConfig.prodCost);
		manager.processDataItemMessage(new ProdConsBoundedBufferConfig.DataItemMessage(prodItem, id));
		itemsProduced += 1;
	}

	void produceData() {
		if (itemsProduced == numItemsToProduce) {
			System.out.println("Producer completed");
			manager.producerExits();
			((PaniniCapsule)manager).shutdown();
		} else {
			produce();
			//System.out.println("Produced "+itemsProduced+" items");
		}
	}
}

capsule ConsumerActor (int id, ManagerActor manager) {
	double consItem = 0.0;

	private void consume(double dataToConsume) {
		consItem = ProdConsBoundedBufferConfig.processItem(consItem + dataToConsume, ProdConsBoundedBufferConfig.consCost);
	}

	void consumeDataItem(ProdConsBoundedBufferConfig.DataItemMessage dm) {
		consume(dm.data);
		manager.processConsumerAvailableMessage(new ProdConsBoundedBufferConfig.ConsumerAvailableMessage(id));
		//System.out.println("Consumed item");
	}
	
	void done() {
		((PaniniCapsule)manager).shutdown();
	}
}

capsule ManagerActor (int bufferSize, int numProducers, int numConsumers, int numItemsPerProducer,
						ProducerActor producers[], ConsumerActor consumers[]) {
	int adjustedBufferSize;
	List<ProdConsBoundedBufferConfig.DataItemMessage> pendingData = new ArrayList<ProdConsBoundedBufferConfig.DataItemMessage>();
	int numTerminatedProducers = 0;
	List<ProducerActor> availableProducers = new ArrayList<ProducerActor>();
	List<ConsumerActor> availableConsumers;
	=>{
		adjustedBufferSize = bufferSize - numProducers;
		//System.out.println("adjustedBufferSize: "+adjustedBufferSize);
		availableConsumers = new ArrayList<ConsumerActor>();
		for(ConsumerActor consumer : consumers) {
			availableConsumers.add(consumer);
		}
	};

	void processDataItemMessage(ProdConsBoundedBufferConfig.DataItemMessage dm) {
		if (availableConsumers.isEmpty()) {
			pendingData.add(dm);
		} else {
			availableConsumers.remove(0).consumeDataItem(dm);
		}
		/*System.out.println("pendingData.size(): "+pendingData.size()+
				"adjustedBufferSize: "+adjustedBufferSize);*/
		if (pendingData.size() >= adjustedBufferSize) {
			availableProducers.add(producers[dm.producerId]);
			//System.out.println("don't come here");
		} else {
			//System.out.println("Produce more");
			producers[dm.producerId].produceData();
		}
	}

	void processConsumerAvailableMessage(ProdConsBoundedBufferConfig.ConsumerAvailableMessage cm) {
		if (pendingData.isEmpty()) {
			availableConsumers.add(consumers[cm.consumerId]);
		} else {
			consumers[cm.consumerId].consumeDataItem(pendingData.remove(0));
			if (!availableProducers.isEmpty()) {
				availableProducers.remove(0).produceData();
			}
		}
	}

	void producerExits() {
		numTerminatedProducers += 1;
		tryExit();
	}

	private void tryExit() {
		if (numTerminatedProducers == numProducers && availableConsumers.size() == numConsumers) {
			for(ConsumerActor consumer : consumers) {
				consumer.done();
			}
			/*for(ProducerActor producer : producers) {
				((PaniniCapsule)producer).shutdown();
			}*/
		}
	}
}

capsule bndbuffer(String[] args) {
	design {
		ProducerActor producers[40];
		ConsumerActor consumers[40];
		ManagerActor manager;
		//producers[0](0, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);
		// do it for others
		//consumers[0](0, manager);
		producers[0](0, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[1](1, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[2](2, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[3](3, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[4](4, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[5](5, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[6](6, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[7](7, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[8](8, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[9](9, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[10](10, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[11](11, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[12](12, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[13](13, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[14](14, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[15](15, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[16](16, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[17](17, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[18](18, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[19](19, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[20](20, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[21](21, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[22](22, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[23](23, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[24](24, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[25](25, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[26](26, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[27](27, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[28](28, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[29](29, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[30](30, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[31](31, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[32](32, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[33](33, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[34](34, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[35](35, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[36](36, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[37](37, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[38](38, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);producers[39](39, manager, ProdConsBoundedBufferConfig.numItemsPerProducer);
		consumers[0](0, manager);consumers[1](1, manager);consumers[2](2, manager);consumers[3](3, manager);consumers[4](4, manager);consumers[5](5, manager);consumers[6](6, manager);consumers[7](7, manager);consumers[8](8, manager);consumers[9](9, manager);consumers[10](10, manager);consumers[11](11, manager);consumers[12](12, manager);consumers[13](13, manager);consumers[14](14, manager);consumers[15](15, manager);consumers[16](16, manager);consumers[17](17, manager);consumers[18](18, manager);consumers[19](19, manager);consumers[20](20, manager);consumers[21](21, manager);consumers[22](22, manager);consumers[23](23, manager);consumers[24](24, manager);consumers[25](25, manager);consumers[26](26, manager);consumers[27](27, manager);consumers[28](28, manager);consumers[29](29, manager);consumers[30](30, manager);consumers[31](31, manager);consumers[32](32, manager);consumers[33](33, manager);consumers[34](34, manager);consumers[35](35, manager);consumers[36](36, manager);consumers[37](37, manager);consumers[38](38, manager);consumers[39](39, manager);

		manager(ProdConsBoundedBufferConfig.bufferSize,
			ProdConsBoundedBufferConfig.numProducers,
			ProdConsBoundedBufferConfig.numConsumers,
			ProdConsBoundedBufferConfig.numItemsPerProducer,
			producers, consumers);
	}

	void run() {
		for(ProducerActor p: producers)
			p.produceData();
	}
}