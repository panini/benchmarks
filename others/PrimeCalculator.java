/*
 * This file is part of the Panini project at Iowa State University.
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/.
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 * 
 * For more details and the latest version of this code please see
 * http://paninij.org
 * 
 * Contributor(s): Ganesha Upadhyaya
 */

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
class NumberRangeMessage implements Serializable
{
    private long startNumber;
    private long endNumber;

    public NumberRangeMessage(long startNumber, long endNumber) {
        this.startNumber = startNumber;
        this.endNumber = endNumber;
    }


    public long getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(long startNumber) {
        this.startNumber = startNumber;
    }

    public long getEndNumber() {
        return endNumber;
    }

    public void setEndNumber(long endNumber) {
        this.endNumber = endNumber;
    }
}

class Result {
	private List<Long> results = new ArrayList<Long>();
	public List<Long> getResults() {
		return results;
	}
}

capsule PrimeWorker {
	Result work(NumberRangeMessage numberRangeMessage) {
		System.out.println("Number Rage: " + numberRangeMessage.getStartNumber() + 
				" to " + numberRangeMessage.getEndNumber());
		// Iterate over the range, compute primes, and return the list of numbers that are prime
		Result result = new Result();
		for(long l = numberRangeMessage.getStartNumber(); 
				l <= numberRangeMessage.getEndNumber(); l++) {
			if(isPrime(l)) {
				result.getResults().add(l);
			}
		}
		return result;
	}
	
	private boolean isPrime(long n) {
		if(n == 1 || n == 2 || n == 3) {
			return true;
		}
		// Is n an even number?
		if(n % 2 == 0) {
			return false;
		}
		//if not, then just check the odds
		for(long i=3; i*i<=n; i+=2) {
			if(n % i == 0) 
				return false;
		}
		return true;
	}
}

capsule PrimeListener {
	void printResults(Result result) {
		System.out.println("Results: ");
		for(Long value : result.getResults()) {
			System.out.print(value + ", ");
		}
		System.out.println();
	}
}

capsule PrimeMaster(PrimeListener listener, PrimeWorker workers[]) {
	void calculate(NumberRangeMessage numberRangeMessage) {
		int numberOfWorkers = workers.length;
		long numberOfNumbers = numberRangeMessage.getEndNumber() - numberRangeMessage.getStartNumber();
		long segmentLength = numberOfNumbers / numberOfWorkers;
		List<Result> results = new ArrayList<Result>();
		Result finalResults = new Result();
		for(int i=0; i<numberOfWorkers; i++) {
			// Compute the start and end numbers for this worker
			long startNumber = numberRangeMessage.getStartNumber() + (i * segmentLength);
			long endNumber = startNumber + segmentLength - 1;
			// Handle any remainder
			if(i == numberOfWorkers - 1) {
				// Make sure we get the rest of the list
				endNumber = numberRangeMessage.getEndNumber();
			}
			// Send a new message to the work router for this subset of numbers
			results.add(workers[i].work(new NumberRangeMessage(startNumber, endNumber)));
		}
		
		for(Result result : results) {
			finalResults.getResults().addAll(result.getResults()); // blocking wait
		}
		listener.printResults(finalResults);
	}
}

capsule PrimeCalculator(String[] args) {
	design {
		PrimeListener listener;
		PrimeMaster master;
		PrimeWorker workers[10];
		master(listener, workers);
	}
	
	void run() {
		if(args.length < 2) {
			System.out.println( "Usage: PrimeCalculator <start-number> <end-number>" );
			System.exit(0);
		}
		long startNumber = Long.parseLong(args[0]);
		long endNumber = Long.parseLong(args[1]);
		calculate(startNumber, endNumber);
	}
	
	private void calculate(long startNumber, long endNumber) {
		master.calculate(new NumberRangeMessage(startNumber, endNumber));
	}
}